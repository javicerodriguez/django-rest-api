# Django REST API with Django Rest Framework

Tutorial by [CodingEntrepeneurs](https://www.youtube.com/watch?v=c708Nf0cHrs)

I build this repository, that contains:

- [x] Backend code (Django & Django Rest Framework)
- [x] Client code for requests to the API (Python with requests lib)
- [ ] Frontend code (React)

In the commits I will explain the steps I took to build this project.